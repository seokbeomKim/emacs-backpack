﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2018 v5.5.153
	 Created on:   	2018-12-05 9:20
	 Created by:   	Sukbeom Kim (sukbeom.kim@gmail.com)
	 Organization: 	https://github.com/seokbeomKim
	 Filename:     	windows.ps1
	===========================================================================
	.DESCRIPTION
		Emacs installation script for Windows.
#>

$VERSION = 0.1

function Unzip ($zipfile, $outpath)
{
	Expand-Archive -Path $zipfile -DestinationPath $outpath -Force
}

function prepareWorkingDirectory () {
	mkdir -Force "C:\EmacsTmp\" | Out-Null
	Set-Location "C:\EmacsTmp\"
}

function downloadFileLink($url)
{
	$filename = Split-Path $url -leaf
	$outpath = Join-Path -Path "C:\EmacsTmp\" -ChildPath $filename
	
	if (![System.IO.File]::Exists($outpath))
	{	
		Invoke-WebRequest -Uri $url -OutFile $outpath.ToString()
	}
	else
	{
		echo "Target file already exists in ${outpath}. So skip to download..."
	}
}

function extractEmacsZipFile ($filename) {
	echo "Extract downloaded emacs zip file to "
	echo "[C:\Emacs\] (type directory path if you want to change): "
	$extractDest = Read-Host
	
	if ($extractDest -eq "")
	{
		$extractDest = "C:\Emacs"
	}
	
	Unzip -zipfile "C:\EmacsTmp\${filename}" -outpath $extractDest
}

function removeTmpFiles ()
{
	Remove-Item -Force "C:\EmacsTmp"
}

function installFromBinary ()
{
	echo 'install from binary...'
	echo "If you want to continue, press any key... or press q to quit"
	
	$read = Read-Host
	if ($read.Equals("q") -or $read.Equals("Q"))
	{
		echo "Go back to main"
		return;
	}
	
	$fileUrl = "https://ftp.gnu.org/gnu/emacs/windows/emacs-26/emacs-26.1-x86_64.zip"
	if (![Environment]::Is64BitProcess)
	{
		$fileUrl = "https://ftp.gnu.org/gnu/emacs/windows/emacs-26/emacs-26.1-i686.zip"
	}
	
	prepareWorkingDirectory
	downloadFileLink($fileUrl)
	extractEmacsZipFile(Split-Path $fileUrl -leaf)
	removeTmpFiles
}

function installFromSource ()
{
	echo 'install from source...'
	echo "If you want to continue, press any key... or press q to quit"
	$read = Read-Host
	if ($read.Equals("q") -or $read.Equals("Q"))
	{
		echo "Go back to main"
		return;
	}
	
	$fileUrl = "http://repo.msys2.org/distrib/x86_64/msys2-x86_64-20180531.exe"
	if (![Environment]::Is64BitProcess)
	{
		$fileUrl = "http://repo.msys2.org/distrib/i686/msys2-i686-20180531.exe"
	}
	prepareWorkingDirectory
	
	$msys2_installDir = ""
	$msys2_installed_flag1 = (@(
			Get-ChildItem HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall |
			% { Get-ItemProperty $_.PsPath } |
			% { echo $_.displayname } |
			Where { $_ -like '*msys*' }).count -gt 0)
	
	if ($msys2_installed_flag1)
	{
		$msys2_installDir = Get-ChildItem HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall |
		% { Get-ItemProperty $_.PsPath } |
		Where { $_.displayname -like '*msys2*' } |
		% { echo $_.installlocation }
	}
	
	$msys2_installed_flag2 = (@(
			Get-ChildItem HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall |
			% { Get-ItemProperty $_.PsPath } |
			% { echo $_.displayname } |
			Where { $_ -like '*msys*' }).count -gt 0)
	
	if ($msys2_installed_flag2)
	{
		$msys2_installDir = Get-ChildItem HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall |
		% { Get-ItemProperty $_.PsPath } |
		Where { $_.displayname -like '*msys2*' } |
		% { echo $_.installlocation }
	}
	
	if (!$msys2_installed_flag1 -and !$msys2_installed_flag2)
	{
		echo '!! You should install MSYS2 first to continue...'
		echo "!! Try to download msys2 install file from ${fileUrl}. File will be ready at C:\EmacsTmp\"
		downloadFileLink($fileUrl)
		
		echo "!! Please install msys2 and retry ... "
	}
	else
	{
		if ($msys2_installDir -eq "")
		{
			# If the script could not find the install directory:
			echo '[ERROR] Cannot find msys installed directory. '
			echo 'If you want to continue, please type the installed directory (like "C:\msys2\"): '
			$msys2_installDir = Read-Host
		}
		
		$msys_exec_filepath = [System.IO.Path]::Combine($msys2_installDir, "msys2_shell.cmd")
		if (![System.IO.File]::Exists($msys_exec_filepath))
		{
			# if the execution file does not exist, halt the script with error message:
			echo "[ERROR] Cannot find ${msys_exec_filepath}"
			return -1
		}
		
		$sprocess = $null
		if ([Environment]::Is64BitProcess)
		{
			$shellpath = [System.IO.Path]::Combine($MyInvocation.PSScriptRoot, "scripts\buildWindowsScript64.sh")
			Start-Process -FilePath $msys_exec_filepath -ArgumentList "-mingw64", $shellpath
		}
		else
		{
			$shellpath = [System.IO.Path]::Combine($MyInvocation.PSScriptRoot, "scripts\buildWindowsScript.sh")
			Start-Process -filepath $msys_exec_filepath -ArgumentList "-mingw32", $shellpath
		}
		
		return
	}
	
}

$ReadOption = $true

while ($ReadOption)
{
	echo "===== Emacs Installation Script v${VERSION} ====="
	echo 'Select a option (input "q" to quit): '
	echo '1) Install Emacs binary from GNU'
	echo '2) Install Emacs from the source'
	
	$INSTALL_OPTION = Read-Host
	
	switch ($INSTALL_OPTION)
	{
		1 {
			installFromBinary
		}
		2 {
			installFromSource
		}
		q {
			return 0
		}
	}
}

Read-Host