#!/bin/bash

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" &&

# use emacs-plus instead of emacs (for me, emacs-plus is better than emacs package).
brew tap d12frosted/emacs-plus
brew install emacs-plus
# brew install emacs --with-cocoa --with-imagemagick@6 --with-librsvg
