#!/bin/bash

echo 'Restore files from backup...'

for file in $(cat refs | sed -e '/^$/d');
do
    targetFile=$(basename $file)
    targetDir=$(dirname $file)
    mkdir -p $targetDir
    cp -r backupFiles/$targetFile $file
done
