#!/bin/bash

targetList=$(cat targets | sed -e '/^#/d')
fileList=()

# Create file lists from 'targets' setting
for target in $targetList;
do
    target=$(echo $target | sed -e 's@~@'"$HOME"'@g')
    target=$(echo $target | sed -e 's@$HOME@'"$HOME"'@g')

    if [ -d $target ]; then
        elFiles=$(find $target -type f -name '*.el')

        for elf in $elFiles;
        do
            fileList+=($elf)
        done

    elif [ -f $target ]; then
        fileList+=($elf)
    fi
done

# Create & initialize backup directories and meta info
echo "" > trefs
rm -rf backupFiles/
mkdir -p backupFiles


# Double quotes
for file in ${fileList[@]};
do
    for i in $(cat $file | sed -e '/;/d' | grep / | awk -F '"' '{print $2}' );
    do
        fpath=$(echo $i | sed -e 's@~@'"$HOME"'@g')
        fpath=$(echo $fpath | sed -e 's@$HOME@'"$HOME"'@g' | sed -e '/.emacs.d/d' | sed -e '/^\.$/d')

        if [ -e $fpath ] && [ ! -z $fpath ]; then
            echo $fpath >> trefs
            cp -r $fpath backupFiles
        fi
    done
done

# Single quotes
for file in ${fileList[@]};
do
    for i in $(cat $file | sed -e '/;/d' | grep / | awk -F \' '{print $2}' );
    do
        fpath=$(echo $i | sed -e 's@~@'"$HOME"'@g')
        fpath=$(echo $fpath | sed -e 's@$HOME@'"$HOME"'@g' | sed -e '/.emacs.d/d' | sed -e '/^\.$/d')

        if [ -e $fpath ] && [ ! -z $fpath ]; then
            echo $fpath >> trefs
            cp -r $fpath backupFiles
        fi
    done
done

# Remove duplicate from trefs
awk '!seen[$0]++' trefs | sed -e '/^$/d' > refs
rm trefs
echo 'Backup Completed'
